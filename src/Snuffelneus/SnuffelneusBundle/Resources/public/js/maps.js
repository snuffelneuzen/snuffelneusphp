var BAD_QUALITY_THRESHOLD = 1823.0;
var MEDIUM_QUALITY_THRESHOLD = 1100.0;

var badQualityIcon = {
    url: '/bundles/snuffelneus/images/bad_small.png',
    size: new google.maps.Size(32, 32),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(0, 32)
};

var mediumQualityIcon = {
    url: '/bundles/snuffelneus/images/medium_small.png',
    size: new google.maps.Size(32, 32),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(0, 32)
};

var goodQualityIcon = {
    url: '/bundles/snuffelneus/images/good_small.png',
    size: new google.maps.Size(32, 32),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(0, 32)
};

$(document).ready(function() {
    var mapOptions = {
        zoom: 10
    };

    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    addMarkers(map);
});

/**
 * Helper function to add markers to a input google.maps.Map (JavaScript API v3)
 * @param map Input map to add markers to.
 */
function addMarkers(map) {
    var userSecret = $("#map-canvas").data("secret");
    var concatenatedUrl = "/detail/" + userSecret + "/readings";

    $.ajax({
        url: concatenatedUrl,
        cache: false
    }).done(function(data) {
        $.each(data, function( index, value ) {
            addCustomMarker(map, value);
        });
    });
}

/**
 * Helper that adds custom markers to the map, given a set of input values and the map.
 * @param map The map to which the marker should be added.
 * @param values The values that should be used for the marker.
 */
function addCustomMarker(map, values) {
    var producedMarker;
    var markerImage;

    if(values.NO2 >= BAD_QUALITY_THRESHOLD) {
        markerImage = badQualityIcon;
    }
    else if((values.NO2 < BAD_QUALITY_THRESHOLD) && (values.NO2 > MEDIUM_QUALITY_THRESHOLD)) {
        markerImage = mediumQualityIcon;
    }
    else {
        markerImage = goodQualityIcon;

    }
    // Create the actual marker.
    producedMarker = new google.maps.Marker({
        position: new google.maps.LatLng(values.Latitude, values.Longitude),
        title: "[" + values.Timestamp + "] NO2: " + values.NO2,
        icon: markerImage
    });

    // Update the click listener for this marker.
    setMarkerEventListener(producedMarker, map);

    // Add marker to the map and center the map on this marker.
    producedMarker.setMap(map);
    map.setCenter(producedMarker.getPosition());
}

/**
 * Helper to set the Marker event listener. Since all marker listeners are the same; we can use one global function.
 * @param producedMarker The marker that should be registered.
 * @param map The map that should register the listener.
 */
function setMarkerEventListener(producedMarker, map) {
    google.maps.event.addListener(producedMarker, 'click', function() {
        var boxText = document.createElement("div");
        boxText.id = 1;
        boxText.style.cssText = "color: #222; font-size: 13px; border: 1px solid white; margin-top: 8px; background: #94BAC3; padding: 10px; border-radius: 3px;";
        boxText.innerHTML = producedMarker.title

        var boxOptions = {
            content: boxText,
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-75, 0),
            zIndex: null,
            boxStyle: {
                width: "150px"
            },
            closeBoxMargin: "10px 2px 2px 5px",
            closeBoxURL: "/bundles/snuffelneus/images/close_small.png",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            pane: "floatPane",
            enableEventPropagation: false
        };

        var infoBox = new InfoBox(boxOptions);
        infoBox.open(map, producedMarker);
    });
}


