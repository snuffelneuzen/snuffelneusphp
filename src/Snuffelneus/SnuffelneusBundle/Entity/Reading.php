<?php

namespace Snuffelneus\SnuffelneusBundle\Entity;

use Snuffelneus\SnuffelneusBundle\Entity\BaseEntity;
use Snuffelneus\SnuffelneusBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="reading")
 */
class Reading extends BaseEntity
{

    /**
     * @ORM\ManyToOne(targetEntity="\Snuffelneus\SnuffelneusBundle\Entity\User", inversedBy="readings")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="\Snuffelneus\SnuffelneusBundle\Entity\Measurement", mappedBy="reading")
     */
    protected $measurements;

    /**
     * @ORM\Column(type="float")
     */
    protected $latitude;

    /**
     * @ORM\Column(type="float")
     */
    protected $longitude;

    /**
     * @return mixed
     */
    public function getMeasurements()
    {
        return $this->measurements;
    }

    /**
     * @param mixed $measurements
     */
    public function setMeasurements($measurements)
    {
        $this->measurements = $measurements;
    }


    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(\Snuffelneus\SnuffelneusBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
}