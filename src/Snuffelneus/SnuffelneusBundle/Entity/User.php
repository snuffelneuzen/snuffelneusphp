<?php

namespace Snuffelneus\SnuffelneusBundle\Entity;

use Snuffelneus\SnuffelneusBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseEntity
{
    /**
     * @ORM\Column(type="string", length=32,unique=true)
     * @var string
     */
    protected $secret;

    /**
     * @ORM\OneToMany(targetEntity="\Snuffelneus\SnuffelneusBundle\Entity\Reading", mappedBy="user")
     */
    protected $readings;

    public function __construct() {
        parent::__construct();
        $readings = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param string $email
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    public function setReadings($readings)
    {
        $this->readings = $readings;

        return $this;
    }

    public function addReading(\Snuffelneus\SnuffelneusBundle\Entity\Reading $readings)
    {
        $this->readings[] = $readings;

        return $this;
    }

    public function removeReading(\Snuffelneus\SnuffelneusBundle\Entity\Reading $measurements)
    {
        $this->readings->removeElement($measurements);
    }

    public function getReadings()
    {
        return $this->readings;
    }
}