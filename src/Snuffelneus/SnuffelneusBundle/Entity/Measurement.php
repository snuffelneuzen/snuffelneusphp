<?php

namespace Snuffelneus\SnuffelneusBundle\Entity;

use \DateTime;
use \Snuffelneus\SnuffelneusBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Measurement, formerly called 'Datum'.
 * Contains a specific type of measurement with a sensor type and its value.
 *
 * @ORM\Entity
 * @ORM\Table("measurements")
 */
class Measurement extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="\Snuffelneus\SnuffelneusBundle\Entity\Reading", inversedBy="measurements")
     */
    private $reading;

    /**
     * @var string
     * @ORM\Column(name="type", type="text")
     */
    private $sensorType;

    /**
     * @var string
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    /**
     * @return string
     */
    public function getSensorType()
    {
        return $this->sensorType;
    }

    /**
     * @param string $sensorType
     */
    public function setSensorType($sensorType)
    {
        $this->sensorType = $sensorType;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Measurement
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function getReading()
    {
        return $this->reading;
    }

    public function setReading($reading)
    {
        $this->reading = $reading;
    }

}
