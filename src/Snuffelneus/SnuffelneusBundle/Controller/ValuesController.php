<?php

namespace Snuffelneus\SnuffelneusBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use \Snuffelneus\SnuffelneusBundle\Entity\User;
use \Snuffelneus\SnuffelneusBundle\Entity\Measurement;
use \Snuffelneus\SnuffelneusBundle\Entity\Reading;
use Symfony\Component\HttpFoundation\Response;

class ValuesController extends Controller
{
    /**
     * Function that handles the GET action for values.
     *
     * @param $secret The Secret for the user.
     * @return JsonResponse The built JSON array for this user
     */
    public function getAction($secret) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('Snuffelneus\SnuffelneusBundle\Entity\User');
        $user = $repository->findOneBy(array('secret' => $secret));

        if($user === null) {
            throw $this->createNotFoundException('No user found with secret ' . $secret);
        }

        $readings = $user->getReadings();

        if($readings === null) {
            throw $this->createNotFoundException('No readings found for user with secret ' . $secret);
        }

        return new JsonResponse($this->buildReadingArray($user, $readings));
    }

    /**
     * Function that responds to a POST request to /api/values/
     * Create a Reading from the post. Requires the following request parameters:
     *
     * Secret: The Secret for the user.
     * Latitude: The Latitude for the taken reading.
     * Longitude: The Longitude for the taken reading
     * Values: An ARRAY of measurements taken during this reading. E.g. Values[0]["Sensor"].
     */
    public function postAction(Request $request)
    {
        $params = array();
        $content = $this->get("request")->getContent();

        if (!empty($content))
        {
            $params = json_decode($content, true); // 2nd param to get as array
        }

        $secret = $params["Secret"];
        $latitude = $params["Latitude"];;
        $longitude = $params["Longitude"];
        $measurements = $params["Values"];

        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('Snuffelneus\SnuffelneusBundle\Entity\User');
        $user = $repository->findOneBy(array('secret' => $secret));

        if($user === null) {
            $user = new User();
            $user->setSecret($secret);
            $em->persist($user);
            $em->flush($user);
        }

        $reading = new Reading();
        $reading->setLatitude($latitude);
        $reading->setLongitude($longitude);
        $reading->setUser($user);

        $em->persist($reading);
        $em->flush($reading);

        foreach($measurements as $measurement) {
            $measurementObj = new Measurement();

            $measurementValue = $measurement["Value"];
            $measurementObj->setSensorType($measurement["Sensor"]);
            $measurementObj->setValue($measurementValue);

            $measurementObj->setReading($reading);

            $em->persist($measurementObj);
            $em->flush($measurementObj);

        }

        return new Response("Success!");
    }

    /**
     * Helper function to build the array of readings to match the 'old' Snuffelneus API.
     * Unfortunately, C#/NewtonSoft JSON builder don't build very similar results from Entities.
     *
     * @param $user The user who's reading are being processed.
     * @param $readings The readings that should be processed into an array.
     * @return array The built array.
     */
    private function buildReadingArray($user, $readings) {
        $returnArray = array();

        foreach($readings as $reading) {

            $readingArray = array();
            $readingArray["Secret"] = $user->getSecret();
            $readingArray["Latitude"] = $reading->getLatitude();
            $readingArray["Longitude"] = $reading->getLongitude();
            $readingArray["Measured"] = $reading->getCreated()->format('Y-m-d H:i:s');
            $readingArray["Values"] = array();

            $readingMeasurements = $reading->getMeasurements();

            foreach($readingMeasurements as $readingMeasurement) {
                $measurementArray = array();
                $measurementArray["SensorType"] = $readingMeasurement->getSensorType();
                $measurementArray["Value"] = $readingMeasurement->getValue();

                array_push($readingArray["Values"], $measurementArray);
            }

            $returnArray[] = $readingArray;
        }

        return $returnArray;
    }
}
