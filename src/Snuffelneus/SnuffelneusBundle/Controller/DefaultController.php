<?php

namespace Snuffelneus\SnuffelneusBundle\Controller;

use Doctrine\Common\Util\Debug;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response The index page.
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('Snuffelneus\SnuffelneusBundle\Entity\User');
        $users = $repository->findAll();

        foreach($users as $user) {
            $userReadings = $this->getUserReadings($user->getId());
            $user->setReadings($userReadings); // This DOES NOT persist to the database -> please don't do this.
        }

        return $this->render('SnuffelneusBundle:Default:index.html.twig', array('users' => $users));
    }

    /**
     * @param $secret User secret
     * @return \Symfony\Component\HttpFoundation\Response Details page for this user.
     */
    public function detailAction($secret) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('Snuffelneus\SnuffelneusBundle\Entity\User');
        $user = $repository->findOneBy(array('secret' => $secret));

        if($user === null) {
            throw $this->createNotFoundException('No user found with secret ' . $secret);
        }

        return $this->render('SnuffelneusBundle:Default:detail.html.twig', array('user' => $user));
    }

    /**
     * Helper to get the latest readings from a specified user.
     *
     * @param $secret User secret.
     * @return JsonResponse Response for this particular action.
     */
    public function latestReadingsAction($secret) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('Snuffelneus\SnuffelneusBundle\Entity\User');
        $user = $repository->findOneBy(array('secret' => $secret));

        if($user === null) {
            throw $this->createNotFoundException('No user found with secret ' . $secret);
        }

        $readings = $user->getReadings();

        if($readings === null) {
            throw $this->createNotFoundException('No readings found for user with secret ' . $secret);
        }

        $readingsArray = $this->toMapsReadingArray($readings);

        return new JsonResponse($readingsArray);
    }

    /**
     * Helper to parse Doctrine readings into an array usable by the Google Maps JavaScript API v3.
     * Converts the Doctrine objects, and puts them into individual arrays which are stripped of unneeded data.
     *
     * @param $readings Input doctrine objects.
     * @return array Parsed/reworked doctrine objects as PHP array.
     */
    private function toMapsReadingArray($readings) {
        $readingsArray = array();
        foreach($readings as $reading) {
            $tempReading = array();
            $tempReading["Longitude"] = $reading->getLongitude();
            $tempReading["Latitude"] = $reading->getLatitude();
            $tempReading["Timestamp"] = $reading->getCreated()->format("d-m-Y H:i");

            $readingMeasurements = $reading->getMeasurements();

            foreach($readingMeasurements as $readingMeasurement) {
                if($readingMeasurement->getSensorType() == "NO2ADC" || $readingMeasurement->getSensorType() == "NO2") {
                    $tempReading["NO2"] = $readingMeasurement->getValue();
                }
            }

            $readingsArray[] = $tempReading;
        }
        return $readingsArray;
    }

    /**
     * Helper to get a limited amount of readings from a User in a specific order.
     *
     * @param $userID The ID of the user for which the readings should be grabbed.
     * @return mixed Readings from this specific user.
     */
    private function getUserReadings($userID) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('Snuffelneus\SnuffelneusBundle\Entity\Reading');

        $query = $repository->createQueryBuilder('reading')
            ->where('reading.user = :user_id')
            ->setParameter('user_id', $userID)
            ->orderBy('reading.created', 'DESC')
            ->setMaxResults(10)
            ->getQuery();

        $readings = $query->getResult();
        return $readings;
    }

}


